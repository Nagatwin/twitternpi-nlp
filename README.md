# TwitterNPI NLP
School project for identifying negative purchase intent on Twitter.  
The objective is to try existing models and to find one working well (and that can be generalized).  
Full project description can be found here : [twitter.pdf](./twitter.pdf).
End of project report can be found here : [main_report.pdf](./Models/main_report.pdf)

![Schema](./TwitterNPI%20NLP.drawio.svg)

# Directory info
## Datasets
Contains datasets used for this project (These may not be included if too large).

## Models
Contains several notebooks used for:  

* Models analysis
* hyperparameters tuning
* Report generation

## Playground
Contains the playground. Used to start playing with the data and new datasets.  
Will be cleaned as the project evolves.

# Schedule and time spent
This project is meant to be finished by the end of Feb. 2020.  

| Period | Time Spent | Note |
| --- | --- | --- |
| 11-04-2019 | 2h | Setting up the git. Getting in touch with the data. |
| 11-07-2019 | 4h | Building the framework for model evaluation and dataset imports. |
| 11-07-2019 | 3h | Adding some models. |
| 11-11-2019 | 2h | Studying the influence of epoch. |
| 11-15-2019 | 2h | Adding comprehensive report generation. |
| 11-20-2019 | 2h | Adding support for cross-validation on f1. |
| 11-20-2019 | 1h | All models are cross-validated on the same folds. |
| 11-20-2019 | 1h | Sequence padding. |
| 11-20-2019 | 2h | Playing with the Steam dataset. |
| 11-21-2019 | 1h | Improving report generation. |
| 11-27-2019 | 2h | Adding support for roc curve and regenerated report. |
| 11-27-2019 | 1h | Fixing fit issue caused by embedding dimension. |
| 12-03-2019 | 6h | Testings on the steam dataset. |
| 12-07-2019 | 2h | Testing regex on the steam dataset. |
| 12-12-2019 | 6h | Adding some blending. |
| 12-12-2019 | 2h | Adding EarlyStopping for Keras. |
| 12-12-2019 | 2h | Cleaning git and notebooks. Creating module files. |
| 12-13-2019 | 1h | Fixing an error in blending. Regenerating report. |
| 12-14-2019 | 1h | Blending on the stemmed dataset. Regenerating report. |
| 12-14-2019 | 1h | Fixing EarlyStopping overfit on test set. |
| 12-14-2019 | 1h | Improving report generation. |
| 01-06-2020 | 1h | Clearing issues. Checking reference. |
| 01-06-2020 | 4h | Checking the embedding dimension. |
| 01-13-2020 | 2h | Cleaning code. |
| 01-13-2020 | 4h | Continuing the work on embedding dimension. |
| 01-13-2020 | 3h | Trying on a balanced Dataset. |
| 01-16-2020 | 2h | Trying steam dataset on colab. |
| 01-17-2020 | 4h | Trying steam dataset on local computer. |
| 01-17-2020 | 1h | Clarifying report. |
| 01-17-2020 | 2h | Regenerating report and blending with new improvements. |
| 01-28-2020 | 4h | Analysis of the correlation of blending estimators. |
| 01-28-2020 | 1h | Cleaning up the blending notebook. |
| 01-29-2020 | 2h | Improved report generation. |
| 01-29-2020 | 6h | Added LSTM/GRU cells analysis. |
| 02-04-2020 | 3h | Regenerating report. Improving Embedding and Blending analysis. |
| 02-09-2020 | 4h | Adding new blending algorithms. |
| 02-11-2020 | 5h | Improving blending analysis. |
| 02-11-2020 | 5h | Working on the final report and graphs. |
| 02-12-2020 | 3h | Working on the final report. |
| 02-12-2020 | 2h | Working on the final report. |

# Author
Made by Guillaume GOYON ( guillaume.goyon@telecom-paris.fr ).  
Supervised by Samed ATOUATI and Mauro SOZIO.

# References
## Finding embedding dimension
Kevin Patel, Pushpak Bhattacharyya : [Towards Lower Bounds on Number of Dimensions for Word Embeddings](https://www.aclweb.org/anthology/I17-2006.pdf)

## Identifying purchase intent on tweets
Martijn OELE : [Identifying Purchase Intentions by Extracting Information from Tweets](https://theses.ubn.ru.nl/bitstream/handle/123456789/4370/Oele%2C_M.J.A._1.pdf?sequence=1)