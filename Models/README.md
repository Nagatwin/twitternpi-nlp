# Models
This directory will have a detailed analysis for each model.  
We used some TF-IDF based models as well as some RNN.  
We also added a blending model. It classifies the prediction of the other models. This gives us a really powerfull model.  
The blending subdirectory saves all the blending predictions so we don't have to re-train every model every time.

## Files
### dataset.py
This file contains a dataset file that is meant to make working with these easier

### models.py
This file contains the necessary framework code to grade keras and sklearn models seamlessly.  
This also makes adding and using new models within the blender much easier.

### notebook_models.ipynb
This notebook contains the base framework code to:  
* grade models
* export a csv summary
* build a md report

### notebook_blending.ipynb
This notebook contains the code to do some blending work on the models evaluated.
It adds the result at the end of the result file.  
It also contains an analysis of the model's performance

### notebook_embed.ipynb
This notebook contains the code to evaluate the influence of the embedding size.

### notebook_result.ipynb
This notebook contains the code to generate a small report from the previous notebooks.

### npi_utils.py
this file contains some utility functions.

## Datasets used
### McDonald's
We used this dataset as it.  

### Stemmed McDonald's
This is a stemmed version of the dataset above.  
The good point of the stemmed version is that there are less different words so a lower vocabulary size hence the learning should be faster.


## Result
So far we monitor the Cross-validation scores of the models (over 10 fold cv), including the mean and variance of those using 2 datasets (mcdonald's and mcdonald's stemmed).  
We moved to the f1 score as these datasets have a highly chance to be skewed.  
We also keep track of the amount of time it took to do the cross-validation process. It shows that some models are way slower than others when it comes to training.  
Results can be seen in the file [result.csv](./result.csv).  
An automatic report is available [report.md](./report.md).  