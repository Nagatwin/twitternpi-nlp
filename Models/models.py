from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import classification_report, log_loss , roc_auc_score, accuracy_score
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC, SVC

from keras.models import Sequential
from keras.layers import Dense, CuDNNLSTM, Bidirectional, Embedding, CuDNNGRU
from keras.callbacks import EarlyStopping
import keras.backend as K

import numpy as np

class ModelHandler:
    def __init__(self):
        return
    
    def build_model(self):
        """
        This builds a new model. To be re-written for each individual model
        """
        return None

    def cross_validate(self, dataset, f):
        """Cross-validate using either dataset and f folds. To be re-written for each framework"""
        return
    
    def fit(self, dataset):
        """fits the model on the dataset. To be re-written for each framework"""
        return
    
    def predict(self, new_data):
        """predicts the data. To be re-written for each framework"""
        return

    def get_info(self):
        """Gets the model information. To be re-written for each framework"""
        return []

class KerasModelHandler(ModelHandler):
    """This model handles keras models"""
    def __init__(self, epoch = 50, embedding_size = 300, cell_size = 64):
        self.cell_size = int(cell_size)
        self.epoch = epoch
        self.embedding_size = embedding_size
        super().__init__()
        return
    
    def parameterize(self, dataset):
        #% Save some parameters
        self.input_length = dataset.sequence_length
        self.vocabulary_size = dataset.vocabulary_size + 1
    
    def cross_validate(self, dataset, folds):
        """Cross-validate using dataset and f folds. For keras functions"""
        self.parameterize(dataset)
        #% Cross-validation
        kfold = StratifiedKFold(n_splits=folds, shuffle=True, random_state=42)

        cv_accuracies = []
        cv_f1s = []
        cv_rocaucs = []
        cv_logloss = []

        for train, test in kfold.split(dataset.X, dataset.y):
            train_ds, test_ds = dataset.split_index(train, test)
            
            self.fit(train_ds)
            soft = self.model.predict(test_ds.X)
            
            y_pred = np.round(soft)
            
            #% Get measures
            ## F1 score for class 1
            cv_f1s.append(classification_report(test_ds.y, y_pred, output_dict=True)['1']['f1-score'])
            
            ## Accuracy
            cv_accuracies.append(self.model.evaluate(test_ds.X, test_ds.y)[1])
            
            #% Log loss
            cv_logloss.append(log_loss(test_ds.y, soft))
            
            #% ROC AUC score
            cv_rocaucs.append(roc_auc_score(test_ds.y, soft))
            
            del self.model
            K.clear_session()
            
        
        return cv_accuracies, cv_f1s, cv_logloss, cv_rocaucs
    
    def fit(self, dataset):
        """fits the model on the dataset. """
        self.parameterize(dataset)
        K.clear_session()
        
        #% Prepare dataset (balance)
        dataset.prepare()
        
        train_ds, early_stopping_ds = dataset.split(0.2)
        
        early_stopping = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
        
        #% Build a new model
        self.model = self.build_model()

        self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        self.model.fit(train_ds.X, train_ds.y, epochs=self.epoch, batch_size=256, validation_data=(early_stopping_ds.X, early_stopping_ds.y), callbacks=[early_stopping])
        return self
    
    def predict(self, new_data):
        """predicts the data."""
        return self.model.predict(new_data.X).T[0]
        
    def get_info(self):
        """Gets the model information. To be re-written for each model"""
        return super().get_info() + ["Keras " + str(self.epoch) + " epochs " + str(self.embedding_size) + " embedding"]
        
class SklearnModelHandler(ModelHandler):
    """This model handles sk-learn models"""
    def cross_validate(self, dataset, folds):
        """Cross-validate using X, y and f folds. For sk-learn functions"""
        #% Cross-validation
        kfold = StratifiedKFold(n_splits=folds, shuffle=True, random_state=42)

        cv_accuracies = []
        cv_rocaucs = []
        cv_logloss = []
        cv_f1s = []

        for train, test in kfold.split(dataset.X, dataset.y):
            train_ds, test_ds = dataset.split_index(train, test)

            self.fit(train_ds)
            soft = self.predict(test_ds)
            
            y_pred = np.round(soft)
            
            #% Get measures
            ## F1 score for class 1
            cv_f1s.append(classification_report(test_ds.y, y_pred, output_dict=True)['1']['f1-score'])
            
            ## Accuracy
            cv_accuracies.append(self.model.score(test_ds.texts, test_ds.y))
            
            #% Log loss
            cv_logloss.append(log_loss(test_ds.y, soft))
            
            #% ROC AUC score
            cv_rocaucs.append(roc_auc_score(test_ds.y, soft))
        
        return cv_accuracies, cv_f1s, cv_logloss, cv_rocaucs
    
    def fit(self, dataset):
        """fits the model on the dataset."""
        
        #% Prepare dataset (balance)
        dataset.prepare()
        
        #% Build a new model
        self.model = self.build_model()

        self.model.fit(dataset.texts, dataset.y)
        return self
    
    def predict(self, new_data):
        """predicts the data. To be re-written for each framework"""
        try:
            return self.model.predict_proba(new_data.texts)[:,1].T
        except:
            return self.model.predict(new_data.texts)
        
    
    def get_info(self):
        """Gets the model information. To be re-written for each model"""
        return super().get_info() + ["Sklearn"]

class BiDirLSTM2ModelHandler(KerasModelHandler):
    """Bi-directional LSTM using 2 levels self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          Bidirectional(CuDNNLSTM(self.cell_size, return_sequences=True)),
          Bidirectional(CuDNNLSTM(self.cell_size)),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Bi Directional LSTM 2 layers " + str(self.cell_size) + " cells"]

class BiDirLSTM1ModelHandler(KerasModelHandler):
    """Bi-directional LSTM using 1 level self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          Bidirectional(CuDNNLSTM(self.cell_size)),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Bi Directional LSTM 1 layer " + str(self.cell_size) + " cells"]

class BiDirGRU2ModelHandler(KerasModelHandler):
    """Bi-directional GRU using 2 levels self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          Bidirectional(CuDNNGRU(self.cell_size, return_sequences=True)),
          Bidirectional(CuDNNGRU(self.cell_size)),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Bi Directional GRU 2 layers " + str(self.cell_size) + " cells"]

class BiDirGRU1ModelHandler(KerasModelHandler):
    """Bi-directional GRU using 1 level self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          Bidirectional(CuDNNGRU(self.cell_size)),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Bi Directional GRU 1 layer " + str(self.cell_size) + " cells"]

class LSTM2ModelHandler(KerasModelHandler):
    """LSTM using 2 levels self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          CuDNNLSTM(self.cell_size, return_sequences=True),
          CuDNNLSTM(self.cell_size),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["LSTM 2 layers " + str(self.cell_size) + " cells"]

class LSTM1ModelHandler(KerasModelHandler):
    """LSTM using 1 level self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          CuDNNLSTM(self.cell_size),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["LSTM 1 layer " + str(self.cell_size) + " cells"]

class GRU2ModelHandler(KerasModelHandler):
    """GRU using 2 levels self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          CuDNNGRU(self.cell_size, return_sequences=True),
          CuDNNGRU(self.cell_size),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["GRU 2 layers " + str(self.cell_size) + " cells"]

class GRU1ModelHandler(KerasModelHandler):
    """GRU using 1 level self.cell_size cells"""
    def build_model(self):
        """This builds a new model."""
        return Sequential([
          Embedding(self.vocabulary_size, self.embedding_size, input_length=self.input_length),
          CuDNNGRU(self.cell_size),
          Dense(1, activation='sigmoid')
          ])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["GRU 1 layer " + str(self.cell_size) + " cells"]

class NaiveBayesModelHandler(SklearnModelHandler):
    def build_model(self):
        """This builds a new model."""
        #% skl classifier, no params needeed
        clf = MultinomialNB()
        #% Text transformation
        cv = CountVectorizer(input='content', analyzer='word')
        #% Create pipeline
        return Pipeline(memory=None, steps=[('transform', cv), ('classify', clf)])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Naive Bayes"]

class LinearSVCModelHandler(SklearnModelHandler):
    def build_model(self):
        """This builds a new model."""
        # Linear clf, increasing max number of iterations to leave it time to converge
        clf = LinearSVC(max_iter=10000)

        #% Text transformation
        cv = CountVectorizer(input='content', analyzer='word')

        return Pipeline(memory=None, steps=[('transform', cv), ('classify', clf)])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Linear SVC"]

class PolySVCModelHandler(SklearnModelHandler):
    def build_model(self):
        """This builds a new model."""
        # Poly clf, increasing max number of iterations to leave it time to converge
        clf = SVC(kernel='poly', max_iter=10000, probability=True, gamma='scale')

        #% Text transformation
        cv = CountVectorizer(input='content', analyzer='word')

        return Pipeline(memory=None, steps=[('transform', cv), ('classify', clf)])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Poly SVC"]

class RBFSVCModelHandler(SklearnModelHandler):
    def build_model(self):
        """This builds a new model."""
        # Poly clf, increasing max number of iterations to leave it time to converge
        clf = SVC(kernel='rbf', max_iter=10000, probability=True, gamma='scale')

        #% Text transformation
        cv = CountVectorizer(input='content', analyzer='word')

        return Pipeline(memory=None, steps=[('transform', cv), ('classify', clf)])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["RBF SVC"]

class RandomForestModelHandler(SklearnModelHandler):
    def build_model(self):
        """This builds a new model."""
        # Poly clf, increasing max number of iterations to leave it time to converge
        clf = RandomForestClassifier(n_estimators = 100)

        #% Text transformation
        cv = CountVectorizer(input='content', analyzer='word')

        return Pipeline(memory=None, steps=[('transform', cv), ('classify', clf)])
    
    def get_info(self):
        """Gets the model information"""
        return super().get_info() + ["Random Forest 100"]