# Report

This is an automatically generated report on the different models.  

## Embedding

This section has graphs featuring the evolution of the product of measure depending on different embedding size

* Accuracy

* Class 1 f1-score

* ROC AUC



### mcdonalds

![score graph mcdonalds](./images/mcdonalds_embed_prod.png)

![score graph mcdonalds zoom](./images/mcdonalds_embed_prod_zoom.png)

### stemmed mcdonalds

![score graph stemmed mcdonalds](./images/stemmed mcdonalds_embed_prod.png)

![score graph stemmed mcdonalds zoom](./images/stemmed mcdonalds_embed_prod_zoom.png)

## Cell Size

This section has graphs featuring the evolution of the product of measure depending on different cells size

* Accuracy

* Class 1 f1-score

* ROC AUC



### mcdonalds

![score graph mcdonalds](./images/mcdonalds_cell_prod.png)

![score graph mcdonalds zoom](./images/mcdonalds_cell_prod_zoom.png)

### stemmed mcdonalds

![score graph stemmed mcdonalds](./images/stemmed mcdonalds_cell_prod.png)

![score graph stemmed mcdonalds zoom](./images/stemmed mcdonalds_cell_prod_zoom.png)

## Comparative tables

This section has comparative tables of the different models using different criterions :

* Accuracy

* Class 1 f1-score

* ROC AUC



For each of these criterion, we also computed the confidence one, based on the 10 fold cross validation measures.

Note that the deep learning models are weak to weight initialisation, and thus these indicators may vary between runs.

There is also a table for training time indications.

### Average accuracy

| model | average accuracy |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.9900 |
| Blending-minimal 80 train 20 blend | 0.9886 |
| Blending 80 train 20 blend | 0.9872 |
| Sklearn Random Forest 100 | 0.9556 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.9552 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.9551 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.9548 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.9546 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.9543 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.9541 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.9535 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.9534 |
| Sklearn Linear SVC | 0.9474 |
| Sklearn Naive Bayes | 0.9402 |

### Confidence accuracy

Calculated using mean_accuracy - 2 * std_accuracy

| model | confidence accuracy |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.9866 |
| Blending-minimal 80 train 20 blend | 0.9850 |
| Blending 80 train 20 blend | 0.9835 |
| Sklearn Random Forest 100 | 0.9515 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.9481 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.9478 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.9477 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.9476 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.9476 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.9471 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.9469 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.9465 |
| Sklearn Linear SVC | 0.9399 |
| Sklearn Naive Bayes | 0.9369 |

### Average f1 score

| model | average f1 score |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.9203 |
| Blending-minimal 80 train 20 blend | 0.9077 |
| Blending 80 train 20 blend | 0.8972 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.6416 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.6372 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.6361 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.6251 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.6215 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.6192 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.6050 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.6043 |
| Sklearn Linear SVC | 0.5920 |
| Sklearn Random Forest 100 | 0.5296 |
| Sklearn Naive Bayes | 0.1656 |

### Confidence f1 score

Calculated using mean_f1 - 2 * std_f1

| model | confidence f1 score |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.8925 |
| Blending-minimal 80 train 20 blend | 0.8783 |
| Blending 80 train 20 blend | 0.8675 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.5730 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.5677 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.5575 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.5567 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.5521 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.5412 |
| Sklearn Linear SVC | 0.5386 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.5336 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.5266 |
| Sklearn Random Forest 100 | 0.4755 |
| Sklearn Naive Bayes | 0.0801 |

### Average roc_auc score

| model | average roc_auc score |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.9956 |
| Blending-minimal 80 train 20 blend | 0.9952 |
| Blending 80 train 20 blend | 0.9951 |
| Sklearn Random Forest 100 | 0.9580 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.9551 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.9513 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.9505 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.9503 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.9493 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.9485 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.9454 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.9442 |
| Sklearn Naive Bayes | 0.8007 |
| Sklearn Linear SVC | 0.7820 |

### Confidence roc_auc score

Calculated using mean_roc_auc - 2 * std_roc_auc

| model | confidence roc_auc score |
| --- | --- |
| Blending-reduced 80 train 20 blend | 0.9922 |
| Blending-minimal 80 train 20 blend | 0.9916 |
| Blending 80 train 20 blend | 0.9914 |
| Sklearn Random Forest 100 | 0.9468 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 0.9336 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 0.9309 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 0.9309 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 0.9299 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 0.9289 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 0.9285 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 0.9244 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 0.9237 |
| Sklearn Naive Bayes | 0.7598 |
| Sklearn Linear SVC | 0.7472 |

### Average training time

| model | average training time |
| --- | --- |
| Sklearn Naive Bayes | 8262 |
| Sklearn Linear SVC | 14768 |
| Keras 50 epochs 300 embedding LSTM 1 layer 64 cells | 115132 |
| Keras 50 epochs 300 embedding GRU 1 layer 64 cells | 115608 |
| Keras 50 epochs 300 embedding GRU 2 layers 64 cells | 129087 |
| Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells | 135830 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells | 149195 |
| Keras 50 epochs 300 embedding LSTM 2 layers 64 cells | 150251 |
| Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells | 188893 |
| Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells | 213798 |
| Sklearn Random Forest 100 | 277569 |
| Blending 80 train 20 blend | nan |
| Blending-minimal 80 train 20 blend | nan |
| Blending-reduced 80 train 20 blend | nan |

Blending training time is missing, because the framework did not permit calculating it under good circumstances. However, it is basically the sum of the training time of the other models.

## Blending 80 train 20 blend

### Model Average

#### F1

F1 : 0.8968 +/- 0.0154

![score graph Blending 80 train 20 blend](./images/0.png)

#### Other

ROC AUC : 0.9951 +/- 0.0018

Log Loss : 0.0470 +/- 0.0042

Accuracy : 0.9871 +/- 0.0019

### Dataset mcdonalds

F1 : 0.8983 +/- 0.0134 (delta: 0.0015)

ROC AUC : 0.9950 +/- 0.0020 (delta: -0.0001)

log loss : 0.0467 +/- 0.0041 (delta: -0.0003)

Accuracy : 0.9873 +/- 0.0016 (delta: 0.0002)

Training time : nan

### Dataset stemmed mcdonalds

F1 : 0.8961 +/- 0.0163 (delta: -0.0007)

ROC AUC : 0.9952 +/- 0.0016 (delta: 0.0001)

log loss : 0.0472 +/- 0.0043 (delta: 0.0002)

Accuracy : 0.9870 +/- 0.0020 (delta: -0.0001)

Training time : nan

## Blending-minimal 80 train 20 blend

### Model Average

#### F1

F1 : 0.9068 +/- 0.0148

![score graph Blending-minimal 80 train 20 blend](./images/1.png)

#### Other

ROC AUC : 0.9952 +/- 0.0017

Log Loss : 0.0470 +/- 0.0036

Accuracy : 0.9885 +/- 0.0018

### Dataset mcdonalds

F1 : 0.9103 +/- 0.0151 (delta: 0.0035)

ROC AUC : 0.9952 +/- 0.0020 (delta: 0.0000)

log loss : 0.0461 +/- 0.0037 (delta: -0.0008)

Accuracy : 0.9889 +/- 0.0018 (delta: 0.0004)

Training time : nan

### Dataset stemmed mcdonalds

F1 : 0.9051 +/- 0.0144 (delta: -0.0018)

ROC AUC : 0.9952 +/- 0.0016 (delta: -0.0000)

log loss : 0.0474 +/- 0.0034 (delta: 0.0004)

Accuracy : 0.9883 +/- 0.0017 (delta: -0.0002)

Training time : nan

## Blending-reduced 80 train 20 blend

### Model Average

#### F1

F1 : 0.9197 +/- 0.0136

![score graph Blending-reduced 80 train 20 blend](./images/2.png)

#### Other

ROC AUC : 0.9956 +/- 0.0017

Log Loss : 0.0441 +/- 0.0034

Accuracy : 0.9900 +/- 0.0017

### Dataset mcdonalds

F1 : 0.9222 +/- 0.0154 (delta: 0.0025)

ROC AUC : 0.9956 +/- 0.0019 (delta: 0.0001)

log loss : 0.0432 +/- 0.0036 (delta: -0.0009)

Accuracy : 0.9903 +/- 0.0019 (delta: 0.0003)

Training time : nan

### Dataset stemmed mcdonalds

F1 : 0.9185 +/- 0.0124 (delta: -0.0012)

ROC AUC : 0.9956 +/- 0.0015 (delta: -0.0000)

log loss : 0.0445 +/- 0.0033 (delta: 0.0004)

Accuracy : 0.9898 +/- 0.0016 (delta: -0.0002)

Training time : nan

## Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells

### Model Average

#### F1

F1 : 0.6251 +/- 0.0377

![score graph Keras 50 epochs 300 embedding Bi Directional GRU 1 layer 64 cells](./images/3.png)

#### Other

ROC AUC : 0.9493 +/- 0.0108

Log Loss : 0.1157 +/- 0.0068

Accuracy : 0.9546 +/- 0.0035

### Dataset mcdonalds

F1 : 0.6337 +/- 0.0330 (delta: 0.0086)

ROC AUC : 0.9471 +/- 0.0120 (delta: -0.0022)

log loss : 0.1177 +/- 0.0077 (delta: 0.0020)

Accuracy : 0.9546 +/- 0.0029 (delta: 0.0001)

Training time : 141183 ms

### Dataset stemmed mcdonalds

F1 : 0.6164 +/- 0.0400 (delta: -0.0086)

ROC AUC : 0.9515 +/- 0.0088 (delta: 0.0022)

log loss : 0.1138 +/- 0.0051 (delta: -0.0020)

Accuracy : 0.9545 +/- 0.0040 (delta: -0.0001)

Training time : 130477 ms

## Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells

### Model Average

#### F1

F1 : 0.6192 +/- 0.0494

![score graph Keras 50 epochs 300 embedding Bi Directional GRU 2 layers 64 cells](./images/4.png)

#### Other

ROC AUC : 0.9505 +/- 0.0104

Log Loss : 0.1163 +/- 0.0071

Accuracy : 0.9548 +/- 0.0036

### Dataset mcdonalds

F1 : 0.5944 +/- 0.0434 (delta: -0.0247)

ROC AUC : 0.9487 +/- 0.0099 (delta: -0.0018)

log loss : 0.1186 +/- 0.0048 (delta: 0.0023)

Accuracy : 0.9535 +/- 0.0033 (delta: -0.0014)

Training time : 191973 ms

### Dataset stemmed mcdonalds

F1 : 0.6439 +/- 0.0422 (delta: 0.0247)

ROC AUC : 0.9522 +/- 0.0106 (delta: 0.0018)

log loss : 0.1140 +/- 0.0081 (delta: -0.0023)

Accuracy : 0.9562 +/- 0.0035 (delta: 0.0014)

Training time : 185814 ms

## Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells

### Model Average

#### F1

F1 : 0.6361 +/- 0.0355

![score graph Keras 50 epochs 300 embedding Bi Directional LSTM 1 layer 64 cells](./images/5.png)

#### Other

ROC AUC : 0.9503 +/- 0.0098

Log Loss : 0.1155 +/- 0.0075

Accuracy : 0.9551 +/- 0.0044

### Dataset mcdonalds

F1 : 0.6453 +/- 0.0374 (delta: 0.0093)

ROC AUC : 0.9502 +/- 0.0109 (delta: -0.0002)

log loss : 0.1154 +/- 0.0081 (delta: -0.0001)

Accuracy : 0.9566 +/- 0.0028 (delta: 0.0015)

Training time : 155252 ms

### Dataset stemmed mcdonalds

F1 : 0.6268 +/- 0.0310 (delta: -0.0093)

ROC AUC : 0.9505 +/- 0.0085 (delta: 0.0002)

log loss : 0.1156 +/- 0.0069 (delta: 0.0001)

Accuracy : 0.9536 +/- 0.0052 (delta: -0.0015)

Training time : 143137 ms

## Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells

### Model Average

#### F1

F1 : 0.6416 +/- 0.0438

![score graph Keras 50 epochs 300 embedding Bi Directional LSTM 2 layers 64 cells](./images/6.png)

#### Other

ROC AUC : 0.9551 +/- 0.0115

Log Loss : 0.1141 +/- 0.0090

Accuracy : 0.9552 +/- 0.0039

### Dataset mcdonalds

F1 : 0.6367 +/- 0.0520 (delta: -0.0050)

ROC AUC : 0.9527 +/- 0.0142 (delta: -0.0023)

log loss : 0.1148 +/- 0.0105 (delta: 0.0007)

Accuracy : 0.9559 +/- 0.0040 (delta: 0.0006)

Training time : 212835 ms

### Dataset stemmed mcdonalds

F1 : 0.6466 +/- 0.0330 (delta: 0.0050)

ROC AUC : 0.9574 +/- 0.0073 (delta: 0.0023)

log loss : 0.1133 +/- 0.0071 (delta: -0.0007)

Accuracy : 0.9546 +/- 0.0037 (delta: -0.0006)

Training time : 214760 ms

## Keras 50 epochs 300 embedding GRU 1 layer 64 cells

### Model Average

#### F1

F1 : 0.6043 +/- 0.0447

![score graph Keras 50 epochs 300 embedding GRU 1 layer 64 cells](./images/7.png)

#### Other

ROC AUC : 0.9485 +/- 0.0101

Log Loss : 0.1252 +/- 0.0091

Accuracy : 0.9535 +/- 0.0036

### Dataset stemmed mcdonalds

F1 : 0.6107 +/- 0.0177 (delta: 0.0064)

ROC AUC : 0.9508 +/- 0.0094 (delta: 0.0023)

log loss : 0.1211 +/- 0.0057 (delta: -0.0041)

Accuracy : 0.9539 +/- 0.0026 (delta: 0.0005)

Training time : 104931 ms

### Dataset mcdonalds

F1 : 0.5980 +/- 0.0601 (delta: -0.0064)

ROC AUC : 0.9462 +/- 0.0102 (delta: -0.0023)

log loss : 0.1292 +/- 0.0100 (delta: 0.0041)

Accuracy : 0.9530 +/- 0.0044 (delta: -0.0005)

Training time : 126284 ms

## Keras 50 epochs 300 embedding GRU 2 layers 64 cells

### Model Average

#### F1

F1 : 0.6050 +/- 0.0329

![score graph Keras 50 epochs 300 embedding GRU 2 layers 64 cells](./images/8.png)

#### Other

ROC AUC : 0.9454 +/- 0.0105

Log Loss : 0.1262 +/- 0.0072

Accuracy : 0.9534 +/- 0.0028

### Dataset mcdonalds

F1 : 0.5969 +/- 0.0297 (delta: -0.0080)

ROC AUC : 0.9448 +/- 0.0097 (delta: -0.0005)

log loss : 0.1266 +/- 0.0076 (delta: 0.0005)

Accuracy : 0.9530 +/- 0.0028 (delta: -0.0004)

Training time : 132224 ms

### Dataset stemmed mcdonalds

F1 : 0.6130 +/- 0.0341 (delta: 0.0080)

ROC AUC : 0.9459 +/- 0.0113 (delta: 0.0005)

log loss : 0.1257 +/- 0.0068 (delta: -0.0005)

Accuracy : 0.9539 +/- 0.0028 (delta: 0.0004)

Training time : 125950 ms

## Keras 50 epochs 300 embedding LSTM 1 layer 64 cells

### Model Average

#### F1

F1 : 0.6215 +/- 0.0320

![score graph Keras 50 epochs 300 embedding LSTM 1 layer 64 cells](./images/9.png)

#### Other

ROC AUC : 0.9442 +/- 0.0107

Log Loss : 0.1197 +/- 0.0067

Accuracy : 0.9541 +/- 0.0032

### Dataset mcdonalds

F1 : 0.6219 +/- 0.0332 (delta: 0.0004)

ROC AUC : 0.9416 +/- 0.0086 (delta: -0.0025)

log loss : 0.1205 +/- 0.0071 (delta: 0.0009)

Accuracy : 0.9539 +/- 0.0034 (delta: -0.0002)

Training time : 118297 ms

### Dataset stemmed mcdonalds

F1 : 0.6211 +/- 0.0308 (delta: -0.0004)

ROC AUC : 0.9467 +/- 0.0118 (delta: 0.0025)

log loss : 0.1188 +/- 0.0062 (delta: -0.0009)

Accuracy : 0.9542 +/- 0.0031 (delta: 0.0002)

Training time : 111967 ms

## Keras 50 epochs 300 embedding LSTM 2 layers 64 cells

### Model Average

#### F1

F1 : 0.6372 +/- 0.0324

![score graph Keras 50 epochs 300 embedding LSTM 2 layers 64 cells](./images/10.png)

#### Other

ROC AUC : 0.9513 +/- 0.0106

Log Loss : 0.1164 +/- 0.0082

Accuracy : 0.9543 +/- 0.0038

### Dataset mcdonalds

F1 : 0.6369 +/- 0.0367 (delta: -0.0002)

ROC AUC : 0.9487 +/- 0.0112 (delta: -0.0026)

log loss : 0.1170 +/- 0.0099 (delta: 0.0005)

Accuracy : 0.9539 +/- 0.0043 (delta: -0.0004)

Training time : 152415 ms

### Dataset stemmed mcdonalds

F1 : 0.6374 +/- 0.0275 (delta: 0.0002)

ROC AUC : 0.9539 +/- 0.0092 (delta: 0.0026)

log loss : 0.1159 +/- 0.0059 (delta: -0.0005)

Accuracy : 0.9547 +/- 0.0032 (delta: 0.0004)

Training time : 148088 ms

## Sklearn Linear SVC

### Model Average

#### F1

F1 : 0.5920 +/- 0.0268

![score graph Sklearn Linear SVC](./images/11.png)

#### Other

ROC AUC : 0.7820 +/- 0.0179

Log Loss : 1.8158 +/- 0.1313

Accuracy : 0.9474 +/- 0.0038

### Dataset mcdonalds

F1 : 0.5937 +/- 0.0272 (delta: 0.0017)

ROC AUC : 0.7832 +/- 0.0212 (delta: 0.0012)

log loss : 1.8075 +/- 0.1099 (delta: -0.0082)

Accuracy : 0.9477 +/- 0.0032 (delta: 0.0002)

Training time : 13484 ms

### Dataset stemmed mcdonalds

F1 : 0.5904 +/- 0.0262 (delta: -0.0017)

ROC AUC : 0.7807 +/- 0.0136 (delta: -0.0012)

log loss : 1.8240 +/- 0.1492 (delta: 0.0082)

Accuracy : 0.9472 +/- 0.0043 (delta: -0.0002)

Training time : 16051 ms

## Sklearn Naive Bayes

### Model Average

#### F1

F1 : 0.1656 +/- 0.0473

![score graph Sklearn Naive Bayes](./images/12.png)

#### Other

ROC AUC : 0.8007 +/- 0.0209

Log Loss : 0.5115 +/- 0.0440

Accuracy : 0.9402 +/- 0.0017

### Dataset mcdonalds

F1 : 0.1464 +/- 0.0492 (delta: -0.0191)

ROC AUC : 0.7966 +/- 0.0208 (delta: -0.0041)

log loss : 0.5258 +/- 0.0423 (delta: 0.0143)

Accuracy : 0.9397 +/- 0.0017 (delta: -0.0005)

Training time : 7892 ms

### Dataset stemmed mcdonalds

F1 : 0.1847 +/- 0.0363 (delta: 0.0191)

ROC AUC : 0.8049 +/- 0.0201 (delta: 0.0041)

log loss : 0.4972 +/- 0.0410 (delta: -0.0143)

Accuracy : 0.9407 +/- 0.0015 (delta: 0.0005)

Training time : 8633 ms

## Sklearn Random Forest 100

### Model Average

#### F1

F1 : 0.5296 +/- 0.0274

![score graph Sklearn Random Forest 100](./images/13.png)

#### Other

ROC AUC : 0.9580 +/- 0.0057

Log Loss : 0.1258 +/- 0.0097

Accuracy : 0.9556 +/- 0.0021

### Dataset mcdonalds

F1 : 0.5299 +/- 0.0314 (delta: 0.0003)

ROC AUC : 0.9573 +/- 0.0066 (delta: -0.0007)

log loss : 0.1255 +/- 0.0106 (delta: -0.0003)

Accuracy : 0.9555 +/- 0.0024 (delta: -0.0002)

Training time : 289810 ms

### Dataset stemmed mcdonalds

F1 : 0.5293 +/- 0.0227 (delta: -0.0003)

ROC AUC : 0.9587 +/- 0.0047 (delta: 0.0007)

log loss : 0.1262 +/- 0.0087 (delta: 0.0003)

Accuracy : 0.9558 +/- 0.0017 (delta: 0.0002)

Training time : 265328 ms