import numpy as np
import matplotlib.pyplot as plt

def examine_cv(cvscores):
    #% Cast scores
    cvscores = np.asarray(cvscores)

    #% Print mean and std
    print("Values: %0.4f (+/- %0.4f)" % (cvscores.mean(), cvscores.std()))

    #% Plot the scores of the cv.
    plt.hist(cvscores)
    plt.show()

    return cvscores.mean(), cvscores.std()

def filter_text(text, filtered=",.:\"()_-\\/"):
    #% Filter characters from text
    text_filtered = text
    for fw in filtered:
        text_filtered = text_filtered.replace(fw, ' ')
    return text_filtered

def remove_double(text):
    #% Filter characters from text
    text_filtered = ""
    last_char = ""
    for c in text:
        if c != last_char:
            last_char = c
            text_filtered += c
    return text_filtered

def df_corr(df, filename):
    #% Displays the correlation matrix of the dataframe
    f = plt.figure(figsize=(19, 15))
    plt.matshow(df.corr(), fignum=f.number)
    plt.xticks(range(df.shape[1]), df.columns, fontsize=14, rotation=90)
    plt.yticks(range(df.shape[1]), df.columns, fontsize=14)
    plt.xlim(-0.5, df.shape[1]-0.5)
    plt.ylim(df.shape[1]-0.5, -0.5)
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=14)
    plt.title('Correlation Matrix', fontsize=16)
    plt.savefig(filename)
    plt.show()

def show_diffs(df, col1, col2, col_true, text):
    #% Prints the differences of tags between 2 columns
    print('1', col1)
    print('2', col2)
    def check(row):
        if round(row[col1]) != round(row[col2]):
            print('1', row[col1], '2', row[col2], 'true', row[col_true], row[text])
    df.apply(check, axis=1)

def save_diffs(df, col1, col2, col_true):
    #% Save the differences of predictions between 2 columns as a new column
    print('1', col1)
    print('2', col2)
    def check(row):
        if round(row[col1]) != round(row[col2]):
            if row[col1] > row[col2] and row[col_true] == 1:
                #% col1 was right
                return 1
            else:
                return -1
        else:
            return 0
    df['res'] = df.apply(check, axis=1)