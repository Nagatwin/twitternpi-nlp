from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import numpy as np

class DataHolder():
    """
    This class is used to hold data.
    It is designed to be easily splitable
    """
    def __init__(self, texts, X, y, vocabulary_size = None, sequence_length = None, is_balanced=False):
        """is_balanced = True : Ensure equal number of class 1 and 0"""
        self.texts = texts
        self.y = y
        self.X = X
        self.vocabulary_size = vocabulary_size
        self.sequence_length = sequence_length
        self.is_balanced = is_balanced
    
    def split(self, ratio):
        """Splits the dataholder based on a ratio (the test size)"""
        X1, X2, texts1, texts2, y1, y2 = train_test_split(self.X, self.texts, self.y, test_size=ratio, random_state=1)
        return DataHolder(texts1, X1, y1, self.vocabulary_size, self.sequence_length, self.is_balanced), \
                DataHolder(texts2, X2, y2, self.vocabulary_size, self.sequence_length, self.is_balanced)
    
    def split_index(self, idx_train, idx_test):
        """Splits the dataholder based on a list of index"""
        return DataHolder(self.texts[idx_train], self.X[idx_train], self.y[idx_train], self.vocabulary_size, self.sequence_length, self.is_balanced), \
                DataHolder(self.texts[idx_test], self.X[idx_test], self.y[idx_test], self.vocabulary_size, self.sequence_length, self.is_balanced)
    
    def prepare(self):
        """Prepares the dataholder. Basically it balances it."""
        if self.is_balanced:
            self.balance()
    
    def balance(self):
        """balances the dataset. After this there is as many class 1 and class 0"""
        ones = np.where(self.y==1)[0]
        zeros = np.where(self.y==0)[0]
        seed = np.random.seed(100)
        final_size_per_class = min(len(ones), len(zeros))
        ones = np.random.choice(ones, final_size_per_class, replace=False)
        zeros = np.random.choice(zeros, final_size_per_class, replace=False)
        indices = np.concatenate((zeros, ones))
        self.texts = self.texts[indices]
        self.X = self.X[indices]
        self.y = self.y[indices]
        
    
class Dataset(DataHolder):
    """
    This class represents and work with a dataset.  
    Sequences are padded at a length so $\approx 99%$ of the lengths are below.
    To do so it estimates it with a gaussian distribution and pick mean+3*std as sequence length.
    """
    def __init__(self, texts, y, name, is_balanced=False):
        """
        Parameters
        ----------
        name : name of the dataset 
        y : labels
        texts : array of texts
        X : tokenized texts
        sequence_length : length of the padded texts
        vocabulary_size : number of words in the tokenizer
        """
        super().__init__(np.array(texts), None, y, is_balanced=is_balanced)
        self.prepare_texts(self.texts)
        self.name = name

    def prepare_texts(self, texts):
        """Prepares a set of texts, displays the histogram of lengths"""
        #% Fit tokenizer
        self.tokenizer = Tokenizer(num_words=None, lower=True, split=' ')
        self.tokenizer.fit_on_texts(texts)

        #% Tokenize data
        texts_tokenized = self.tokenizer.texts_to_sequences(texts)

        #% Lengths of the sequences
        lengths = np.array(sorted([len(entry) for entry in texts_tokenized]))

        #% Vocabulary size
        self.vocabulary_size = len(self.tokenizer.word_index) + 1
        
        mean, std = lengths.mean(), lengths.std()
        
        #% Pad at 99% review distrib len
        self.sequence_length = int(mean + 3 * std)
        
        self.X = pad_sequences(texts_tokenized, maxlen=self.sequence_length)