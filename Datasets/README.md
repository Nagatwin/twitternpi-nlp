# Datasets

## MacDonalds
[File included](./macdonalds_for_guillaume.csv)  
This is the first dataset used. These are Tweets tagged with a negative purchase intent.

## Steam dataset
[File to be downloaded](https://zenodo.org/record/1000885#.Xb_m-0N7luQ)  
This dataset is based on Steam reviews.
